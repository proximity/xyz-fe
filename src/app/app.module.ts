import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LazyLoadImageModule } from 'ng-lazyload-image';
import { ToastrModule } from 'ngx-toastr';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { CompanyComponent } from './components/company/company.component';
import { CompanyService } from './services/company.service';
import { MachineComponent } from './components/machine/machine.component';
import { MachineService } from './services/machine.service';
import { ItemComponent } from './components/item/item.component';
import { ItemService } from './services/item.service';
import { SaleComponent } from './components/sale/sale.component';
import { SaleService } from './services/sale.service';
import { AlertComponent } from './components/alert/alert.component';
import { AlertService } from './services/alert.service';
import { NotificationService } from './services/notification.service';
import { SoldItemComponent } from './components/sold-item/sold-item.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    CompanyComponent,
    MachineComponent,
    ItemComponent,
    SaleComponent,
    AlertComponent,
    SoldItemComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    LazyLoadImageModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
  ],
  providers: [
    CompanyService,
    MachineService,
    ItemService,
    SaleService,
    AlertService,
    NotificationService,
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
