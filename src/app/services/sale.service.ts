import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable()
export class SaleService {
  constructor(private httpClient: HttpClient) {}

  getSales() {
    let saleApiEndpoint;

    if (environment.production) {
      saleApiEndpoint = `${environment.aws}/api/sale`;
    } else {
      saleApiEndpoint = `${environment.localhost}/api/sale`;
    }

    return this.httpClient.get(saleApiEndpoint);
  }

  getSale(id) {
    let saleApiEndpoint;

    if (environment.production) {
      saleApiEndpoint = `${environment.aws}/api/sale/${id}`;
    } else {
      saleApiEndpoint = `${environment.localhost}/api/sale/${id}`;
    }

    return this.httpClient.get(saleApiEndpoint);
  }
}
