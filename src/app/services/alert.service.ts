import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable()
export class AlertService {
  constructor(private httpClient: HttpClient) {}

  getAlerts() {
    let alertApiEndpoint;

    if (environment.production) {
      alertApiEndpoint = `${environment.aws}/api/alert`;
    } else {
      alertApiEndpoint = `${environment.localhost}/api/alert`;
    }

    return this.httpClient.get(alertApiEndpoint);
  }
}
