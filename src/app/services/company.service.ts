import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable()
export class CompanyService {
  constructor(private httpClient: HttpClient) {}

  getCompanies() {
    let companyApiEndpoint;

    if (environment.production) {
      companyApiEndpoint = `${environment.aws}/api/company`;
    } else {
      companyApiEndpoint = `${environment.localhost}/api/company`;
    }

    return this.httpClient.get(companyApiEndpoint);
  }
}
