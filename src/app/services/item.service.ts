import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable()
export class ItemService {
  constructor(private httpClient: HttpClient) {}

  getItems() {
    let itemApiEndpoint;

    if (environment.production) {
      itemApiEndpoint = `${environment.aws}/api/item`;
    } else {
      itemApiEndpoint = `${environment.localhost}/api/item`;
    }

    return this.httpClient.get(itemApiEndpoint);
  }
}
