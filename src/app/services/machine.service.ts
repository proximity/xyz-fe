import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable()
export class MachineService {
  constructor(private httpClient: HttpClient) {}

  getMachines() {
    let machineApiEndpoint;

    if (environment.production) {
      machineApiEndpoint = `${environment.aws}/api/machine`;
    } else {
      machineApiEndpoint = `${environment.localhost}/api/machine`;
    }

    return this.httpClient.get(machineApiEndpoint);
  }

  withdrawMoney(id) {
    let machineApiEndpoint;

    if (environment.production) {
      machineApiEndpoint = `${environment.aws}/api/machine/withdrawMoney/${id}`;
    } else {
      machineApiEndpoint = `${environment.localhost}/api/machine/withdrawMoney/${id}`;
    }

    return this.httpClient.get(machineApiEndpoint);
  }
}
