import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

@Injectable()
export class NotificationService {
  constructor(private toastrService: ToastrService) {}

  showSuccess(message, title) {
    this.toastrService.success(message, title, {
      closeButton: true,
      progressBar: true,
    });
  }

  showError(message, title) {
    this.toastrService.error(message, title, {
      closeButton: true,
      progressBar: true,
    });
  }
}
