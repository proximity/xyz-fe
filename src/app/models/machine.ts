export class Machine {
  id: any;
  model: any;
  money: any;
  company: any;
  imagePath: any;

  constructor(machine: any) {
    this.id = machine.id;
    this.model = machine.model;
    this.money = machine.money;
    this.company = machine.company;
    this.imagePath =
      machine.model === 'XYZ1'
        ? 'assets/images/machine/XYZ1.jpg'
        : 'assets/images/machine/XYZ2.png';
  }
}
