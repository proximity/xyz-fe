export class Item {
  code: any;
  item: any;
  price: any;
  quantity: any;
  imagePath: any;

  constructor(item: any) {
    this.code = item.code;
    this.item = item.item;
    this.price = item.price;
    this.quantity = item.quantity;

    switch (item.item) {
      case 'Carne de res en bistec':
        this.imagePath = 'assets/images/items/bistec.png';
        break;
      case 'Carne de res molida':
        this.imagePath = 'assets/images/items/carne-molida.png';
        break;
      case 'Chorizo y longaniza':
        this.imagePath = 'assets/images/items/chorizo.jpg';
        break;
      case 'Pollo rostizado':
        this.imagePath = 'assets/images/items/pollo-rostizado.jpg';
        break;
      case 'Jamón':
        this.imagePath = 'assets/images/items/jamon.png';
        break;
      case 'Plátano tabasco':
        this.imagePath = 'assets/images/items/platano-tabasco.jpg';
        break;
      case 'Refrescos de cola y de sabores':
        this.imagePath = 'assets/images/items/refresco-cola.jpg';
        break;
      case 'Limón':
        this.imagePath = 'assets/images/items/limon.png';
        break;
      case 'Naranja':
        this.imagePath = 'assets/images/items/naranja.jpg';
        break;
      case 'Agua embotellada':
        this.imagePath = 'assets/images/items/agua-embotellada.jpg';
    }
  }
}
