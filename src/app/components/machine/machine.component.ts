import { Component, OnInit } from '@angular/core';
import { MachineService } from 'src/app/services/machine.service';
import { Machine } from 'src/app/models/machine';

@Component({
  selector: 'app-machine',
  templateUrl: './machine.component.html',
  styleUrls: ['./machine.component.scss'],
})
export class MachineComponent implements OnInit {
  machines = [];

  constructor(private machineService: MachineService) {}

  ngOnInit(): void {
    this.loadMachines();
  }

  loadMachines() {
    this.machineService.getMachines().subscribe(
      (data: any) => {
        data.forEach((machine) => this.machines.push(new Machine(machine)));
      },
      (error) => {
        console.log(error);
      }
    );
  }
}
