import { Component, OnInit } from '@angular/core';
import { SaleService } from 'src/app/services/sale.service';
import { ActivatedRoute } from '@angular/router';
import { Item } from 'src/app/models/item';

@Component({
  selector: 'app-sold-item',
  templateUrl: './sold-item.component.html',
  styleUrls: ['./sold-item.component.scss'],
})
export class SoldItemComponent implements OnInit {
  id: number;
  soldItems = [];

  constructor(
    private activatedRoute: ActivatedRoute,
    private saleService: SaleService
  ) {}

  ngOnInit(): void {
    this.activatedRoute.params.subscribe((params) => {
      this.id = params.id;
    });
    this.getSale(this.id);
  }

  getSale(id) {
    this.saleService.getSale(id).subscribe(
      (data: any) => {
        data.soldItems.forEach((item) => this.soldItems.push(new Item(item)));
      },
      (error) => {
        console.log(error);
      }
    );
  }
}
