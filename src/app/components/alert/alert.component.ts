import { Component, OnInit } from '@angular/core';
import { AlertService } from 'src/app/services/alert.service';
import { NotificationService } from 'src/app/services/notification.service';
import { MachineService } from 'src/app/services/machine.service';

@Component({
  selector: 'app-alert',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.scss'],
})
export class AlertComponent implements OnInit {
  alerts = [];

  constructor(
    private alertService: AlertService,
    private notificationService: NotificationService,
    private machineService: MachineService
  ) {}

  ngOnInit(): void {
    this.loadAlerts();
  }

  loadAlerts() {
    this.alertService.getAlerts().subscribe(
      (data: any) => {
        this.alerts = data;
      },
      (error) => {
        console.log(error);
      }
    );
  }

  withdrawMoney(id) {
    this.machineService.withdrawMoney(id).subscribe(
      (data) => {
        this.notificationService.showSuccess(
          'The money has been collected',
          'Withdraw Money'
        );
        this.loadAlerts();
      },
      (error) => {
        this.notificationService.showError(
          'Please try later',
          'An error occurred'
        );
      }
    );
  }
}
