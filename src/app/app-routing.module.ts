import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CompanyComponent } from './components/company/company.component';
import { MachineComponent } from './components/machine/machine.component';
import { ItemComponent } from './components/item/item.component';
import { SaleComponent } from './components/sale/sale.component';
import { AlertComponent } from './components/alert/alert.component';
import { SoldItemComponent } from './components/sold-item/sold-item.component';

const routes: Routes = [
  {
    path: '',
    component: CompanyComponent,
  },
  {
    path: 'company',
    component: CompanyComponent,
  },
  {
    path: 'machine',
    component: MachineComponent,
  },
  {
    path: 'item',
    component: ItemComponent,
  },
  {
    path: 'sale',
    component: SaleComponent,
  },
  {
    path: 'alert',
    component: AlertComponent,
  },
  {
    path: 'sale/soldItem/:id',
    component: SoldItemComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
